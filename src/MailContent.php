<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 */

namespace Reelworx\TYPO3\MailService;

final class MailContent
{
    public string $subject;
    public string $text;
    public string $html;

    public function __construct(string $subject = '', string $text = '', string $html = '')
    {
        $this->subject = $subject;
        $this->text = $text;
        $this->html = $html;
    }
}
