<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 */

namespace Reelworx\TYPO3\MailService;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Throwable;

final class MailMessage extends \TYPO3\CMS\Core\Mail\MailMessage implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * Send the message and track if errors occurred
     */
    public function send(): bool
    {
        $accepted = false;
        try {
            $accepted = parent::send();
        } catch (Throwable $e) {
            $data['message'] = [
                'to' => $this->getTo() ? $this->getTo()[0]->toString() : '[not-set]',
                'subject' => $this->getSubject(),
            ];
            $data['exception'] = $e;
            $this->logger->critical('Email sending caused exception', $data);
        }
        return $accepted;
    }

    /**
     * Shorthand method to set mail content based on pre-rendered data
     *
     * @param MailContent $mailContent
     * @return $this
     */
    public function setContent(MailContent $mailContent): self
    {
        $this->subject($mailContent->subject);
        if ($mailContent->text) {
            $this->text($mailContent->text);
        }
        if ($mailContent->html) {
            $this->html($mailContent->html);
        }
        return $this;
    }
}
