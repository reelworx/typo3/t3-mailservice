<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 */

namespace Reelworx\TYPO3\MailService;

use TYPO3\CMS\Core\Utility\GeneralUtility;

final class MailConfiguration
{
    public string $senderEmail = '';
    public string $senderName = '';
    public string $cc = '';
    public string $bcc = '';
    public string $replyTo = '';
    public string $organization = '';

    public string $extensionName = '';
    public string $controllerName = '';
    public string $pluginName = '';

    /**
     * @var string[]
     */
    public array $templatePaths = [];
    /**
     * @var string[]
     */
    public array $layoutPaths = [];
    /**
     * @var string[]
     */
    public array $partialPaths = [];

    /**
     * @var string[]
     */
    public array $allowedLanguages = [];

    public bool $setAutoSubmittedHeader = true;

    /**
     * @param array{
     *     sender_name: ?string,
     *     sender_email: ?string,
     *     recipient_copy: ?string,
     *     replyTo: ?string,
     *     organization: ?string,
     *     languages: ?string,
     *     view: array{
     *         templateRootPaths: ?string[],
     *         layoutRootPaths: ?string[],
     *         partialRootPaths: ?string[]
     *     }
     * } $settings
     */
    public static function fromArray(array $settings): self
    {
        $config = new self();
        $config->senderName = $settings['sender_name'] ?? '';
        $config->senderEmail = $settings['sender_email'] ?? '';
        $config->bcc = $settings['recipient_copy'] ?? '';
        $config->replyTo = $settings['replyTo'] ?? '';
        $config->organization = $settings['organization'] ?? '';

        $config->templatePaths = $settings['view']['templateRootPaths'] ?? [];
        $config->layoutPaths = $settings['view']['layoutRootPaths'] ?? [];
        $config->partialPaths = $settings['view']['partialRootPaths'] ?? [];

        $config->allowedLanguages = array_filter(GeneralUtility::trimExplode(',', $settings['languages'] ?? ''));

        return $config;
    }

    /**
     * Check whether the provided language is allowed and return it.
     * Otherwise, use first allowed language.
     *
     * @param string $iso2
     * @return string
     */
    public function validatedLanguage(string $iso2): string
    {
        if ($this->allowedLanguages && !in_array($iso2, $this->allowedLanguages, true)) {
            $iso2 = reset($this->allowedLanguages);
        }
        return $iso2;
    }
}
