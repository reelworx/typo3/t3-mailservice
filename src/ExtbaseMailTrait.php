<?php

declare(strict_types=1);

namespace Reelworx\TYPO3\MailService;

use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\CMS\Fluid\View\StandaloneView;

trait ExtbaseMailTrait
{
    protected function createMailMessage(
        ?MailService &$mailService,
        ?StandaloneView &$mailView,
        ?MailMessage &$message
    ): void {
        $config = MailConfiguration::fromArray($this->settings['mail']);

        /** @var Request $request */
        $request = $this->request;
        $config->extensionName = $request->getControllerExtensionName();
        $config->pluginName = $request->getPluginName();
        $config->controllerName = $request->getControllerName();

        $mailService = new MailService($config);
        $mailService->injectEventDispatcher(GeneralUtility::makeInstance(EventDispatcherInterface::class));

        $message = $mailService->createMessage();

        $mailView = $mailService->createMailView($message, $request);
        $mailView->assign('settings', $this->settings);
    }
}
