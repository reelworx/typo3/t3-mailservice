#!/usr/bin/env bash

TOOL_DIR=.Build/tools/phpstan
TOOL_PACKAGE="phpstan/phpstan:^2 phpstan/phpdoc-parser phpstan/phpstan-strict-rules bnf/phpstan-psr-container"
TOOL_COMMAND="phpstan analyse --verbose --no-progress"

source scripts/runphptool.sh
