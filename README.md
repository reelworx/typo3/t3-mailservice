# TYPO3 mail service

This library can be used by TYPO3 extensions to have a convenient toolset to send email.

## Features

* Fluid-based mail templates for HTML and TEXT format
* Multiple languages (including fallback)
* Usable in all contexts (frontend, backend, CLI)
* Image embedding via ViewHelpers
* Post-render event to modify the mail content even more

## Mail template requirements

Each mail requires at least one of the two template files:
* The HTML template: Stored in `<language>/<ControllerName>/<mailName>.html` 
* The TEXT template: Stored in `<language>/<ControllerName>/<mailName>.txt`

Where:
* `<language>` is a ISO-2 language code as configured in the site configuration (lower case)
* `<ControllerName>` is the name of the current controller as configured with `$mailConfig->controllerName` (see below)
* `<mailName>` is freely chosen name for the email

Both files are Fluid templates! (within the txt format no HTML escaping is performed)\\
Both files need to use UNIX line endings.

Example templates can be found in the `template-examples` folder of this library.

### Mail subject

The template's **first lines** hold the _subject_ of the mail.
The subject line may be blank in one template if you use both templates. 

## Usage within your extension

```php
function sendMail()
{ 
    // optional in non-frontend environment
    // if your mail needs to generate links to a frontend site, you need to fake a FE environment with:
    // \Reelworx\TYPO3\FakeFrontend\FrontendUtility::buildFakeFE(<uid of some frontend page>);

    // assuming TypoScript settings holds all config  (see reference below)
    $mailConfig = MailConfiguration::fromArray($this->settings['mail']);

    $mailConfig->extensionName = 'MyExtension';
    $mailConfig->controllerName = 'Controller';
    $mailConfig->pluginName = 'MyPlugin';

    $mailService = GeneralUtility::makeInstance(MailService::class, $mailConfig);
    $msg = $mailService->createMessage();
    $mailView = $mailService->createMailView($msg);

    $mailView->assign('mydata', 'somedata');

    $msg->setContent($mailService->renderMail($mailView, '<mailName>'));
    $msg->setTo($someRecipient);

    // optional:
    $msg->attachFromPath($someFalFile->getForLocalProcessing(false));

    $msg->send();
}
```

### Extbase controllers

Within Extbase controllers the setup is simplified by using the provided trait.

**The important assumption:** The mail configuration can be found in `$this->settings['mail']`

```php
class FooController extends ActionController
{
   use \Reelworx\TYPO3\MailService\ExtbaseMailTrait;

   /* ... */
   public function myAction(): void
   {
       // optional in non-frontend environment
       // if your mail needs to generate links to a frontend site, you need to fake a FE environment with:
       // \Reelworx\TYPO3\FakeFrontend\FrontendUtility::buildFakeFE(<uid of some frontend page>);
   
       //
       // Assumption: TypoScript settings 'mail' holds all mail config (see reference below)
       //
       
       /** @var MailService $mailService */
       /** @var MailMessage $msg */
       /** @var StandaloneView $mailView */
       $this->createMailMessage($mailService, $mailView, $msg);
       
       $mailView->assign('mydata', 'somedata');
       
       $msg->setContent($mailService->renderMail($mailView, '<mailName>'));
       
       $msg->setTo($someRecipient);
       
       // optional:
       $msg->attachFromPath($someFalFile->getForLocalProcessing(false));

       $msg->send();
   }
}
```

## Post render event to change the mail content

You may optionally equip the `MailService` with the Core's EventDispatcher,
which needs to be injected manually. (The `ExtbaseMailTrait` already takes care of this for you.)

This allows you to use the `MailRenderedEvent`.

Register your event handler for the `MailRenderedEvent` (See [TYPO3 documentation](https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/Events/EventDispatcher/Index.html#implementing-an-event-listener-in-your-extension))

The event has a getter to retrieve the `MailContent` object, which you may modify to your needs.

_Example usage:_ Modify the HTML content with some CSS inliner or similar processing tool to get great mail layout quality.
Checkout [MJML](https://mjml.io/) for instance.

## Configuration reference

If you configure the mail settings via TypoScript (which you should in order to use the `ExtbaseMailTrait`)
this is how your settings should look like:

```typo3_typoscript
plugin.tx_yourext.settings.mail {
    view {
        templateRootPaths.10 = EXT:yourext/Resources/Private/Mail/Templates/
        layoutRootPaths.10 = EXT:yourext/Resources/Private/Mail/Layouts/
        partialRootPaths.10 = EXT:yourext/Resources/Private/Mail/Partials/
    }
    
    // the following settings are optional
    sender_name = company website
    sender_email = noreply@example.com
    recipient_copy = bcc@example.com
    replyTo = office@example.com
    // this will be the Organization header of messages
    organization = The Company

    // optional: Define allowed languages; The first listed language is used as fallback
    // e.g. Always send mails in EN no matter which language the website run with
    languages = en,de
}
```
