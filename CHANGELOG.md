# Change log

## Version 4.0.0

* Dropped TYPO3 v11 support
* Added TYPO3 v13 support

## Version 3.0.0

* Dropped TYPO3 v10 support
* Added TYPO3 v12 support

## Version 2.4.0

* Feature: Added `render-language` variable to mail view. (2-letter ISO-code in lowercase, "default" for "en")

## Version 2.3.1

* Fix: Handle empty "allowedLanguages" setting correctly

## Version 2.3.0

* Feature: Added configuration option `setAutoSubmittedHeader` (defaults to `true`) to set necessary mail headers avoiding out-of-office responses

## Version 2.2.0

* Fix: Use HTML and plain text version from same folder, if either is present. No format-individual fallback.
* Feature: Allow to limit allowed languages

## Version 2.1.1

* Subject of mail is not overwritten by HTML's subject if it's empty

## Version 2.1.0

* Use newer reelworx/t3-fakefrontend version

## Version 2.0.0

* TYPO3 v11 support
* Feature: Post render event
* Feature/Breaking: Subject is loaded from HTML or plain text template
* Feature: Auto-generate plain text version if the template is empty
* Feature: Added ViewHelper to embed an image and retrieve its CID

## Version 1.0.0

Initial version
